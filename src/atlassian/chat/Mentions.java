package atlassian.chat;

import java.util.regex.Pattern;

/*
 *  A way to mention a user. Always starts with an '@' and ends when hitting a non-word character.
 *  (http://help.hipchat.com/knowledgebase/articles/64429-how-do-mentions-work-)
 */
public class Mentions extends MetaTextElements {
	private static final String  PLURAL_NAME = "mentions";
	private static final Pattern PATTERN = Pattern.compile("@(\\w+)");

	/*
	 * (non-Javadoc)
	 * @see atlassian.chat.MetaTextElements#getPattern()
	 */
	@Override
	public Pattern getPattern() {
		return PATTERN;
	}	

	/*
	 * (non-Javadoc)
	 * @see atlassian.chat.MetaTextElements#getCollectionName()
	 */
	@Override
	public String getCollectionName() {
		return PLURAL_NAME;
	}	
}
