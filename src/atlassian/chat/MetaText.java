package atlassian.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/*
 * collection of operations on MetaText such as mentions & emoticons
 */
public class MetaText {
	public static final String EOL = System.getProperty("line.separator");
	
	/*
	 * read chat messages from standard in, write json meta text to standard out
	 */
	public static void main(String[] args) {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Please enter chat messages:");
		
		try {
			while (true) {
				System.out.println(
					parseText(in.readLine())
				);
			}
		} catch (IOException ioe) {
			System.out.println("IOException reading commands");
			System.exit(1);
		}
	}

	/*
	 * parses meta elements out of text
	 * 
	 * @param text chat text message to parse
	 * @return json meta text generated from chat message
	 */
	public static String parseText(String text) {
		List<MetaTextElements> elements = getAllElements();
		
		// apply the various regular expressions to find the meta elements
		for(MetaTextElements e : elements) {
			e.match(text);
		}

		// serialize the meta elements as json
		// TODO - use a third part library for json (un)marshalling (e.g. Jackson)
		StringBuffer returnJson = new StringBuffer();
		returnJson.append("{").append(EOL);
		for(MetaTextElements e : elements) {
			e.toJson(returnJson);
		}
		
		//remove the erroneous trailing comma
		int lastComma = returnJson.lastIndexOf("],");
		if(lastComma > 0) {
			returnJson.deleteCharAt(lastComma+1);
		}
		
		returnJson.append("}").append(EOL);
		return returnJson.toString();
	}
	
	/*
	 * returns a list of all registered MetaTextElements
	 * 
	 * @return list of registered MetaTextElements
	 */
	private static List<MetaTextElements> getAllElements() {
		List<MetaTextElements> elements = new ArrayList<MetaTextElements>(3);
		
		// register the different varieties of meta text
		if(elements.isEmpty()) {
			elements.add(new Mentions());
			elements.add(new CustomEmoticons());
			elements.add(new Links());
		}
		
		return elements;
	}
}
