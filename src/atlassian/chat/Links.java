package atlassian.chat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 *  URLs contained in the chat text messages, along with the page's title.
 */
public class Links extends MetaTextElements {
	private static final int MAX_TITLE_LENGTH = 47;
	private static final String PLURAL_NAME = "links";
	private static final String ELEMENT_FORMAT = "    %s%s%n";
	private static final String LINK_FORMAT = "{%n      \"url\": \"%s\",%n      \"title\": \"%s\"%n    }";
	private static final Pattern PATTERN = Pattern.compile("(?i)(http[s]?://[^\\s]+\\.[^\\s]+)");
	private static final Pattern HEAD_PATTERN = Pattern.compile("(?i)</head>");
	private static final Pattern TITLE_PATTERN = Pattern.compile("(?i)<title.*>(.*)</title>");

	/*
	 * {
	 *   "url": "http://www.nbcolympics.com",
	 *   "title": "NBC Olympics | 2014 NBC Olympics in Sochi Russia"
	 * }
	 */
	private class Link {
		private String url;
		private String title;
		
		Link(String url, String title) {
			this.url = url;
			this.title = title;
		}
		
		public String toString() {
			return String.format(LINK_FORMAT, url, title);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see atlassian.chat.MetaTextElements#getPattern()
	 */
	@Override
	public Pattern getPattern() {
		return PATTERN;
	}	

	/*
	 * (non-Javadoc)
	 * @see atlassian.chat.MetaTextElements#getCollectionName()
	 */
	@Override
	public String getCollectionName() {
		return PLURAL_NAME;
	}	

	/*
	 * (non-Javadoc)
	 * @see atlassian.chat.MetaTextElements#getElementFormat()
	 */
	@Override
	public String getElementFormat() {
		return ELEMENT_FORMAT;
	}

	/*
	 * (non-Javadoc)
	 * @see atlassian.chat.MetaTextElements#match(java.lang.String)
	 */
	@Override
	public void match(String chatMessage) {
		Matcher m = getPattern().matcher(chatMessage);
		
		while(m.find()) {
			String url = m.group(GROUP);
		    String title = "";
		    
		    title = getTitle(url);
			
			elements.add(new Link(url, title));
		}
	}
	
	/*
	 * retrieve the title of a given url
	 * 
	 * @param urlString url formatted as a String
	 */
	private String getTitle(String urlString) {
		StringBuffer lines = new StringBuffer();
		String title = "";

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					(new URL(urlString)).openStream()
			));

			for(String line=reader.readLine();line != null;line = reader.readLine()) {
				lines.append(line);
				
				if(HEAD_PATTERN.matcher(line).find()) {
					// we've read the entire head, time to find the title
					break;
				}
			}
			reader.close();

			// convert all whitespace (e.g. CRLF) to plain space
			lines.toString().replaceAll("\\s+", " ");
			
			Matcher matcher = TITLE_PATTERN.matcher(lines);
			
			if(matcher.find()) {
				title = matcher.group(1).trim();
				if(title.length() > MAX_TITLE_LENGTH) {
					title = title.substring(0, MAX_TITLE_LENGTH)+"...";
				}
			}
		} catch(Exception e) {
			// unable to retrieve a title; not the end of the world...
		}

		return title;
	}
}