package atlassian.chat;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Base class for collections of meta text elements.
 * 
 * Shared function includes repeated application of pattern matching and serialization.
 * Shared interface leveraged in collections of various subclasses & template method style calls to subclass overrides
 * 
 * This may be a little OO..Overkill.
 */
public abstract class MetaTextElements {
	public static final int GROUP = 1;
	
	private static final String COLLECTION_FORMAT = "  \"%s\": [%n";
	private static final String ELEMENT_FORMAT = "    \"%s\"%s%n";
	
	protected ArrayList elements = new ArrayList(3);
	
	/*
	 * retrieves regex pattern used to find meta text elements 
	 */
	public abstract Pattern getPattern();
	
	/*
	 * retrieves the name (plural noun) of the collection 
	 */
	public abstract String getCollectionName();

	/*
	 * retrieves format string to be used with String.format
	 * 
	 * default implementation for the most basic of elements:  String
	 */
	public String getElementFormat() {
		return ELEMENT_FORMAT;
	}
	
	/*
	 * uses the Pattern from getPattern to search the chatMessage argument adding
	 * patterns it finds to its element collection
	 * 
	 * side effect - modifies the state of the internal elements collection 
	 * 
	 *  @param chatMessage messsage to search for Pattern-s
	 */
	public void match(String chatMessage) {
		Matcher m = getPattern().matcher(chatMessage);
		
		while(m.find()) {
			elements.add(m.group(GROUP));
		}
	}
	
	/*
	 * walk over the elements adding them to the json collection
	 * 
	 * @param returnJson StringBuffer to use as the return value - contains the json serialization of meta text elements
	 */
	public void toJson(StringBuffer returnJson) {
		if(!elements.isEmpty()) {
			returnJson.append(String.format(COLLECTION_FORMAT, getCollectionName()));
			for(int i=0; i < elements.size()-1; i++) {
				returnJson.append(String.format(getElementFormat(), elements.get(i), ","));
			}
			returnJson.append(String.format(getElementFormat(), elements.get(elements.size()-1), ""));
			returnJson.append("  ],").append(MetaText.EOL);
		}
	}
}
