package atlassian.chat;

import java.util.regex.Pattern;

/*
 *  'custom' emoticons which are ASCII strings, no longer than 15 characters, contained in parenthesis.
 *  anything matching this format is an emoticon. (http://hipchat-emoticons.nyh.name)
 */
public class CustomEmoticons extends MetaTextElements {
	private static final String  PLURAL_NAME = "emoticons";
	private static final Pattern PATTERN = Pattern.compile("\\((\\w{1,15}+)\\)");

	/*
	 * (non-Javadoc)
	 * @see atlassian.chat.MetaTextElements#getPattern()
	 */
	@Override
	public Pattern getPattern() {
		return PATTERN;
	}	

	/*
	 * (non-Javadoc)
	 * @see atlassian.chat.MetaTextElements#getCollectionName()
	 */
	@Override
	public String getCollectionName() {
		return PLURAL_NAME;
	}	
}
