package atlassian.chat.test;

import static org.junit.Assert.*;
import org.junit.Test;
import atlassian.chat.MetaText;

public class MetaTextTest {

	/* 
	 * Input: "@chris you around?"
	 * Return (string):
	 * {
	 *   "mentions": [
	 *     "chris"
	 *   ]
	 * }
	 */
	@Test
	public void atMentionShouldReturnMetions() {
		String jsonMetaText = MetaText.parseText("@chris @jon @stannis you around?");
		// System.out.println(jsonMetaText);		
		String expectedResult = 
				"{"+MetaText.EOL+
				"  \"mentions\": ["+MetaText.EOL+
				"    \"chris\","+MetaText.EOL+
				"    \"jon\","+MetaText.EOL+
				"    \"stannis\""+MetaText.EOL+
				"  ]"+MetaText.EOL+
				"}"+MetaText.EOL;
		// System.out.println(expectedResult);		
		assertEquals(expectedResult, jsonMetaText);
	}

	@Test
	public void atMentionAsFinalMessageEntry() {
		String jsonMetaText = MetaText.parseText("ping @rob");
		// System.out.println(jsonMetaText);		
		String expectedResult = 
				"{"+MetaText.EOL+
				"  \"mentions\": ["+MetaText.EOL+
				"    \"rob\""+MetaText.EOL+
				"  ]"+MetaText.EOL+
				"}"+MetaText.EOL;
		// System.out.println(expectedResult);		
		assertEquals(expectedResult, jsonMetaText);
	}

	/*
	 * Input: "Good morning! (megusta) (coffee)"
	 * Return (string):
	 * {
	 *   "emoticons": [
	 *     "megusta",
	 *     "coffee"
	 *   ]
	 * }
	 */
	@Test
	public void customEmoticonsShouldReturnEmoticons() {
		String jsonMetaText = MetaText.parseText("Good morning! (megusta) (coffee)");
		// System.out.println(jsonMetaText);		
		String expectedResult = 
				"{"+MetaText.EOL+
				"  \"emoticons\": ["+MetaText.EOL+
				"    \"megusta\","+MetaText.EOL+
				"    \"coffee\""+MetaText.EOL+
				"  ]"+MetaText.EOL+
				"}"+MetaText.EOL;
		// System.out.println(expectedResult);		
		assertEquals(expectedResult, jsonMetaText);
	}

	@Test
	public void customEmoticonsShouldBeBetweenOneAndFifteenCharacters() {
		String jsonMetaText = MetaText.parseText("before (1) between (0123456789ABCED) after");
		// System.out.println(jsonMetaText);		
		String expectedResult = 
				"{"+MetaText.EOL+
				"  \"emoticons\": ["+MetaText.EOL+
				"    \"1\","+MetaText.EOL+
				"    \"0123456789ABCED\""+MetaText.EOL+
				"  ]"+MetaText.EOL+
				"}"+MetaText.EOL;
		// System.out.println(expectedResult);		
		assertEquals(expectedResult, jsonMetaText);
	}

	@Test
	public void customEmoticonsShouldNotBeSixteenCharacters() {
		String jsonMetaText = MetaText.parseText("before (0123456789ABCEDE) after");
		// System.out.println(jsonMetaText);		
		String expectedResult = 
				"{"+MetaText.EOL+
				"}"+MetaText.EOL;
		// System.out.println(expectedResult);		
		assertEquals(expectedResult, jsonMetaText);
	}

	/*
	 * Input: "Olympics are starting soon; http://www.nbcolympics.com"
	 * Return (string):
	 * {
	 *   "links": [
	 *     {
	 *       "url": "http://www.nbcolympics.com",
	 *       "title": "NBC Olympics | 2014 NBC Olympics in Sochi Russia"
	 *     }
	 *   ]
	 * }
	 */
	@Test
	public void urlsShouldReturnLinks() {
		String jsonMetaText = MetaText.parseText("Olympics are starting soon; http://www.nbcolympics.com HTTP://www.olympic.org/");
		// System.out.println(jsonMetaText);
		String expectedResult = 
				"{"+MetaText.EOL+
				"  \"links\": ["+MetaText.EOL+
				"    {"+MetaText.EOL+
                "      \"url\": \"http://www.nbcolympics.com\","+MetaText.EOL+
                "      \"title\": \"NBC Olympics | Home of the 2016 Olympic Games i...\""+MetaText.EOL+
				"    },"+MetaText.EOL+
				"    {"+MetaText.EOL+
                "      \"url\": \"HTTP://www.olympic.org/\","+MetaText.EOL+
                "      \"title\": \"Olympics | Olympic Games, Medals, Results, News...\""+MetaText.EOL+
				"    }"+MetaText.EOL+
				"  ]"+MetaText.EOL+
				"}"+MetaText.EOL;
		// System.out.println(expectedResult);		
		assertEquals(expectedResult, jsonMetaText);
	}

	/*
	 * Input: "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
	 * Return (string):
	 * {
	 *   "mentions": [
	 *     "bob",
	 *     "john"
	 *   ],
	 *   "emoticons": [
	 *     "success"
	 *   ]
	 *   "links": [
	 *     {
	 *       "url": "https://twitter.com/jdorfman/status/430511497475670016",
	 *       "title": "Twitter / jdorfman: nice @littlebigdetail from ..."
	 *     }
	 *   ]
	 * }
	 */
	@Test
	public void validExamplesShouldReturnValidJson() {
		String jsonMetaText = MetaText.parseText("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016");
		// System.out.println(jsonMetaText);
		String expectedResult = 
				"{"+MetaText.EOL+
				"  \"mentions\": ["+MetaText.EOL+
				"    \"bob\","+MetaText.EOL+
				"    \"john\""+MetaText.EOL+
				"  ],"+MetaText.EOL+
				"  \"emoticons\": ["+MetaText.EOL+
				"    \"success\""+MetaText.EOL+
				"  ],"+MetaText.EOL+
				"  \"links\": ["+MetaText.EOL+
				"    {"+MetaText.EOL+
                "      \"url\": \"https://twitter.com/jdorfman/status/430511497475670016\","+MetaText.EOL+
                "      \"title\": \"Justin Dorfman on Twitter: &quot;nice @littlebi...\""+MetaText.EOL+
				"    }"+MetaText.EOL+
				"  ]"+MetaText.EOL+
				"}"+MetaText.EOL;
        // System.out.println(expectedResult);		
		assertEquals(expectedResult, jsonMetaText);
	}
}